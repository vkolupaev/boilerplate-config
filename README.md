boilerplate-config
=======

![GitLab License](
https://img.shields.io/gitlab/license/vkolupaev/boilerplate-config?color=informational
)
![GitLab tag (latest by SemVer)](
https://img.shields.io/gitlab/v/tag/vkolupaev/boilerplate-config?label=boilerplate-config
)

## What is this repository?
...

## ⭐ If I saved you time or helped you in some way
All code in the repo is open source and completely free. I don't need anything from you.

Just please put a star ⭐ if it was helpful to you. This motivates me to further develop this and other OpenSource
projects.

## To whom and how can it be useful?
...

## Ok, how to use it?
...

## I have an error or have a question or idea💡. What to do?
There are two options:
1. Create an Issue and describe the situation.
2. Please feel free to write to me. My contacts are listed below 👇.

---

Copyright 2022 [Viacheslav Kolupaev](
https://vkolupaev.com/?utm_source=readme&utm_medium=link&utm_campaign=boilerplate-config
).

[![website](
https://img.shields.io/static/v1?label=website&message=vkolupaev.com&color=blueviolet&style=for-the-badge&
)](https://vkolupaev.com/?utm_source=readme&utm_medium=badge&utm_campaign=boilerplate-config)

[![LinkedIn](
https://img.shields.io/static/v1?label=LinkedIn&message=vkolupaev&color=informational&style=flat&logo=linkedin
)](https://www.linkedin.com/in/vkolupaev/)
[![Telegram](
https://img.shields.io/static/v1?label=Telegram&message=@vkolupaev&color=informational&style=flat&logo=telegram
)](https://t.me/vkolupaev/)
